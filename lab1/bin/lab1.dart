import 'dart:io';

import 'package:lab1/lab1.dart' as lab1;

void main(List<String> arguments) {
  String? words = stdin.readLineSync()!;
  words.toLowerCase();
  List<String> wordList = words.split(' ');
  var seen = Set<String>();
  List<String> uniqueWordList =
      wordList.where((word) => seen.add(word)).toList();
  for (var word in uniqueWordList) {
    print(word);
  }
}
